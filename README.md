# Istio on Google K8S Engine

A workshop of 3 hours max to learn about Istio and how to use it on Google Cloud Platform (GCP) through Google Kubernetes Engine (GKE).

## Getting started

### Gitlab runner
```
Λ\: $ gitlab-runner exec docker build
```

### Docker
```
Λ\: $ docker run --init -it --rm -u $UID --privileged -v $(pwd):/antora antora/antora generate site.yml --to-dir dist --clean
```

### NPM
```
Λ\: $ npm i -g @antora/cli @antora/site-generator-default

Λ\: $ antora generate site.yml --to-dir dist --clean
```
