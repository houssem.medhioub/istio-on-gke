package com.stacklabs.workshop.istioongke.middleware

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import java.time.ZonedDateTime

class DatabaseService(wcb: WebClient.Builder, prop: MiddlewareProperties) {

    private val databaseUri = prop.databaseUri

    private val log = LoggerFactory.getLogger(DatabaseService::class.java)
    private val wc = wcb.baseUrl(databaseUri.toASCIIString()).build()

    fun call(): Mono<Message> {

        log.info("Before call to DatabaseService at url $databaseUri")

        return wc.get()
                .retrieve()
                .bodyToMono<Message>()
                .doOnSuccess { log.info("Call made to $databaseUri") }
    }
}

data class Message(
        val from: String,
        val date: ZonedDateTime = ZonedDateTime.now()
)
