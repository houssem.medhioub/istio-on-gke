package com.stacklabs.workshop.istioongke.middleware

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.time.Duration.ofMillis
import java.time.ZonedDateTime.now

@Configuration
@EnableConfigurationProperties(MiddlewareProperties::class)
@Import(MiddlewareHandler::class, DatabaseService::class)
class ApplicationConfiguration {
    @Bean
    fun routes(middleware: MiddlewareHandler) = router {
        GET("/", middleware::serve)
    }
}

class MiddlewareHandler(
        private val database: DatabaseService,
        prop: MiddlewareProperties
) {

    private val log = LoggerFactory.getLogger(MiddlewareHandler::class.java)

    private val name = prop.name
    private val version: String = prop.version
    private val requestWaitingRange = (0..prop.maxLatency).map(Int::toLong)

    val errorProbability = 0..100
    val errorRate = prop.errorRate

    @SuppressWarnings("")
    fun serve(@Suppress("UNUSED_PARAMETER") r: ServerRequest): Mono<ServerResponse> {

        val duration = ofMillis(requestWaitingRange.shuffled().first())

        val error = errorProbability.shuffled().first() >= errorRate

        return if (error) {
            Mono.just(1)
                .delayElement(duration)
                .flatMap { database.call() }
                .delayElement(duration)
                .map { it.copy(from = "$name ($version) => ${it.from}", date = now()) }
                .doOnNext { log.info("$name service in version $version called and answered with $it") }
                .flatMap { ServerResponse.ok().bodyValue(it) }
                .doOnSubscribe { log.info("UI Service in version $version starting...") }
        } else {
            log.error("Random error 🔥")
            ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Random error 🔥").toMono()
        }
    }
}
