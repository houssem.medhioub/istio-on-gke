* **Workshop** ⚔️

**      xref:01_cluster-setup.adoc[1. Cluster setup]
***     xref:01_cluster-setup.adoc#kubernetes-engine[Kubernetes Engine]
***     xref:01_cluster-setup.adoc#cluster-creation[Cluster creation]
****    xref:01_cluster-setup.adoc#cluster-creation-cli[With `gcloud`]
****    xref:01_cluster-setup.adoc#cluster-creation-ui[Within Cloud Console]

**      xref:02_istio-within-gke.adoc[2. Istio within GKE]
***     xref:02_istio-within-gke.adoc#istio-namespace[`istio` namespace]
****    xref:02_istio-within-gke.adoc#_services[Services]
****    xref:02_istio-within-gke.adoc#_pods[Pods]
***     xref:02_istio-within-gke.adoc#stackdriver[Stackdriver]
****    xref:02_istio-within-gke.adoc#_trace[Trace]
****    xref:02_istio-within-gke.adoc#_logging[Logging]
****    xref:02_istio-within-gke.adoc#_monitoring[Monitoring]

**      xref:03_application-bootstrap.adoc[3. Application bootstrap]
***     xref:03_application-bootstrap.adoc#anatomy[Anatomy]
***     xref:03_application-bootstrap.adoc#deploy[Deploy]
***     xref:03_application-bootstrap.adoc#generate-traffic[Generate traffic]
***     xref:03_application-bootstrap.adoc#traces[Traces]
***     xref:03_application-bootstrap.adoc#logging[Logging]

**      xref:04-production-path.adoc[4. Production path]
***     xref:04-production-path.adoc#mirroring[Mirroring]
***     xref:04-production-path.adoc#canary-release[Canary release]
***     xref:04-production-path.adoc#traffic-splitting[Traffic Splitting]

**      xref:05-resiliency.adoc[5. Resiliency]
***     xref:05-resiliency.adoc#retries[Retries]
***     xref:05-resiliency.adoc#traffic-limiting[Traffic Limiting]
***     xref:05-resiliency.adoc#pool-ejection[Pool Ejection]

**      xref:06-fault-injection.adoc[6. Fault Injection]
***     xref:06-fault-injection.adoc#injecting-errors[Injecting Errors]
***     xref:06-fault-injection.adoc#delay[Delay]
***     xref:06-fault-injection.adoc#timeout[Timeout]

