= Deploy a configured GKE cluster
:experimental:

Let's begin with the instantiation of a Kubernetes cluster with a managed version of Istio in Google Kubernetes Engine.

[#kubernetes-engine]
== Kubernetes engine

Kubernetes Engine is Google's managed service for running Kubernetes clusters. You can access it's dedicated section
through the main menu as shown:

image:01_cluster-setup/open-gke-clusters.jpeg[Open GKE clusters page]


[#cluster-creation]
== Cluster creation

IMPORTANT: You may *either* create the cluster within the *cloud console* or using the *command line*. Since it takes a few
minutes for the cluster to be up and running, *we recommend you to go with the command line* at first. While waiting for
the job to be done, you can take a look at the UI and how to spawn a cluster from there.


[#cluster-creation-cli]
=== Using the `gcloud` command line interface

To open the Google Cloud Shell, click on the button located in the console header as shown:

image:01_cluster-setup/open-cloud-shell.jpeg[Open Google Cloud Shell]

IMPORTANT: You will need your **project ID** to perform those commands. If running in Google Cloud Shell, it is
available in the *`DEVSHELL_PROJECT_ID`* environment variable.

[source,bash]
----
# If running outside of the Cloud Shell, uncomment next line and replace "nlo-gcp-k8s-istio-5233238d" by your project ID
# Λ\: $ export DEVSHELL_PROJECT_ID=nlo-gcp-k8s-istio-5233238d

Λ\: $ gcloud config set project "${DEVSHELL_PROJECT_ID}"

Λ\: $ gcloud beta container clusters create "istio-formation" \
      --zone "europe-north1-a" \
      --cluster-version "1.13.12-gke.25" \
      --machine-type "n1-standard-2" \
      --image-type "COS" \
      --num-nodes "5" \
      --enable-stackdriver-kubernetes \
      --enable-ip-alias \
      --addons HorizontalPodAutoscaling,HttpLoadBalancing,Istio \
      --istio-config auth=MTLS_PERMISSIVE

WARNING: Starting in 1.12, default node pools in new clusters will have their legacy Compute Engine instance metadata endpoints disabled by default. To create a cluster with legacy instance metadata endpoints disabled in the default node pool, run `clusters create` with the flag `--metadata disable-legacy-endpoints=true`.
WARNING: The Pod address range limits the maximum size of the cluster. Please refer to https://cloud.google.com/kubernetes-engine/docs/how-to/flexible-pod-cidr to learn how to optimize IP address allocation.
This will enable the autorepair feature for nodes. Please see https://cloud.google.com/kubernetes-engine/docs/node-auto-repair for more information on node autorepairs.
Creating cluster istio-formation in europe-north1-a... Cluster is being health-checked (master is healthy)...done.
Created [https://container.googleapis.com/v1beta1/projects/nlo-gcp-k8s-istio-5233238d/zones/europe-north1-a/clusters/istio-formation].
To inspect the contents of your cluster, go to: https://console.cloud.google.com/kubernetes/workload_/gcloud/europe-north1-a/istio-formation?project=nlo-gcp-k8s-istio-5233238d
kubeconfig entry generated for istio-formation.

NAME             LOCATION         MASTER_VERSION  MASTER_IP    MACHINE_TYPE   NODE_VERSION    NUM_NODES  STATUS
istio-formation  europe-north1-a  1.13.11-gke.14  35.228.3.95  n1-standard-2  1.13.11-gke.14  5          RUNNING
----

[#cluster-creation-ui]
=== Within Google Cloud Console

CAUTION: If you created the cluster using the command line in the previous section, you *may skip this chapter* and
go directly to xref:02_istio-within-gke.adoc[the next one].

1. Click on btn:[Create cluster]
+
image:01_cluster-setup/click-create-cluster.jpeg[Click on create cluster]

2. You will need:
* A `Standard cluster` template
* A name: `istio-formation`
* A location: `europe-north1-a`
* A version of GKE: just pick the `default` one
+
image:01_cluster-setup/template-name-version-location.jpeg[Pick template, location, name and GKE version]

3. Before looking into the additional features, you need to set a few parameters in the `Node pools` section:
* Set the **Number of Nodes** to `5`
* Pick a `2 vCPus / 7.5 GB memory` **Machine Type**
+
image:01_cluster-setup/pool-options.jpeg[Size and pick a machine type]

4. Once the additional features are opened, verify that you have the following **Networking** options enabled:
* VPC Native (using alias IP)
* HTTP load balancing
+
image:01_cluster-setup/network-options.jpeg[Select the correct networking fields]

5. Finally, before clicking on btn:[Create], you will need:
* To enable *Stackdriver Kubernetes Engine Monitoring*
* To enable *Istio* in the `Permissive` mode
+
image:01_cluster-setup/monitoring-istio-create.jpeg[Setup Stackdriver and create cluster]

The cluster creation may take a few moments. Once finished, you need to establish a connection. Click on the
btn:[Connect] button next to the cluster in the console.

image:01_cluster-setup/wait-cluster.jpeg[Wait for cluster readyness then click on connect]

* Copy/paste the command inside Cloud Shell or in your terminal.

image:01_cluster-setup/connect-cluster.jpeg[Connect to the cluster]

[.next-page]
xref:02_istio-within-gke.adoc[🠖 Sail to next page ⛵]