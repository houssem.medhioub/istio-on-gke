= You said Istio ?

image:04_you-said-istio/pirate-thinking.gif[Thinking pirate,source=https://media.giphy.com/media/2xW0s2ZYjKbq8/giphy.gif]

This section aims to share some resources we stumbled upon while preparing this workshop. We believe those can be
interesting in case you want to get deeper into Istio's features.

* The link:https://istio.io/[official documentation, window="_blank"] (which is truly amazing 😍)
* A link:https://www.youtube.com/watch?v=gdAG4xnx71c&feature=emb_logo[Youtube walktrough, window="_blank"] of the latest
 features from release 1.4
* A nice link:https://blog.wescale.fr/2019/10/24/istio-sur-gke/[blog post (fr-FR), window="_blank"] explaining the
 basics of Istio in GKE
* A completelink:https://medium.com/faun/istio-step-by-step-part-01-introduction-to-istio-b9fd0df30a9e[step-by-step tutorial, window="_blank"]
 with more gazing at Istio's architecture

In any case, you don't need to spend time reading those. *Let's jump right into the workshop !*

[.next-page]
xref:istio-on-gke:02_workshop:01_cluster-setup.adoc[🠖 Sail to next page ⛵]