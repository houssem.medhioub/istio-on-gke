#!/usr/bin/env python3
import os
import re
import string
import random
import argparse
from multiprocessing.dummy import Pool as ThreadPool

import google.auth
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from google.oauth2 import service_account


class WorkEnvironment:
    @staticmethod
    def from_csv(csv_line: str, email_column: int = 0, sep: str = ',', project_column: int = None):
        email = csv_line.split(sep)[email_column]

        project = None
        if project_column is not None:
            project = csv_line.split(sep)[project_column]

        return WorkEnvironment(email=email, project=project)

    @staticmethod
    def __generate_string(size: int) -> str:
        return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(size))

    def __init__(self, email: str, project: str = None):
        self.email = email
        if project is None:
            project = self.__generate_project_id()
        self.project = project

    def __generate_project_id(self) -> str:
        split = self.email.split('@')
        if len(split) < 2:
            raise Exception(f'"{self.email}" is not an email address')
        user_name = split[0]
        prefix = ''.join(char if re.match("^[A-Za-z0-9]*$", char) else '-' for char in user_name)
        # Remove duplicate following "-" chars
        new_prefix = ''
        for char in prefix:
            if new_prefix == '' and char != '-':
                new_prefix = char
                continue
            if len(new_prefix) > 0:
                if new_prefix[-1] != '-' or char != '-':
                    new_prefix += char
        if new_prefix[-1] == '-':
            new_prefix = new_prefix[:-1]
        return '%.30s' % f'{new_prefix}-{self.__generate_string(30)}'


def ensure_project_existence(
        environment: WorkEnvironment,
        organization: str,
        folder: str = None,
        credentials: google.oauth2.service_account.credentials.Credentials = None
):
    service = discovery.build('cloudresourcemanager', 'v1beta1', credentials=credentials)
    request = service.projects().create(body={
        'name': environment.project,
        'projectId': environment.project,
        'parent': {
            'type': 'folder' if folder is not None else 'organization',
            'id': folder if folder is not None else organization
        }
    })

    print(f'👷 About to create a project called "{environment.project}"')
    try:
        request.execute()
        print('✅ Done')
    except HttpError as err:
        if err.resp.status == 409:
            print(f'😅 Project "{environment.project}" already exists')
        else:
            raise Exception(f'unknown error: {err}')


def associate_billing_account(
        environment: WorkEnvironment,
        billing_account: str,
        credentials: google.oauth2.service_account.credentials.Credentials = None
):
    service = discovery.build('cloudbilling', 'v1', credentials=credentials)
    billing_request = service.projects().updateBillingInfo(
        name=f'projects/{environment.project}',
        body={
          'projectId': environment.project,
          'billingAccountName': f'billingAccounts/{billing_account}',
          "billingEnabled": True
        }
    )

    print(f'👷 About to associate billing account "{billing_account}" to project "{environment.project}"')
    billing_request.execute()
    print('✅ Done')


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='''
            Create project(s), affect a billing account and save the association(s) into a file
        '''
    )
    parser.add_argument(
        dest='input_path',
        help='Path to input CSV file',
    )
    parser.add_argument(
        '-o', '--output-path',
        dest='output_path',
        help='Path to the output path. Defaults to `output.csv` in the working directory',
        default=os.path.join(os.getcwd(), 'output.csv'),
    )
    parser.add_argument(
        '--email-col',
        dest='email_col',
        help='Index of the email column. Defaults to 0',
        default=0,
        type=int
    )
    parser.add_argument(
        '--project-col',
        dest='project_col',
        help='Index of the project column if any',
        type=int
    )
    parser.add_argument(
        '--separator',
        dest='separator',
        help='Separator to use to parse the CSV. Defaults to ","',
        default=','
    )
    parser.add_argument(
        '-s', '--service-account-key',
        dest='service_account_key',
        help='Path to the GCP service account key to use',
        required=True
    )
    parser.add_argument(
        '--billing-account',
        dest='billing_account',
        help='Billing account ID to associate the project(s) to',
        required=True
    )
    parser.add_argument(
        '--organization',
        dest='organization',
        help='Organization to associate the project(s) to',
        required=True
    )
    parser.add_argument(
        '--folder',
        dest='folder',
        help='Folder to affect the project(s) to in any',
    )
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        help='Number of threads to use to parallelize requests. Defaults to 5',
        type=int,
        default=5
    )
    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_arguments()

    with open(os.path.abspath(arguments.input_path), 'r') as stream:
        content = stream.readlines()

    gcp_credentials = service_account.Credentials.from_service_account_file(
        filename=os.path.abspath(arguments.service_account_key)
    )

    pool = ThreadPool(arguments.threads)
    environments = [
        WorkEnvironment.from_csv(
            csv_line=raw.strip(),
            email_column=arguments.email_col,
            sep=arguments.separator,
            project_column=arguments.project_col
        )
        for raw in content
    ]

    def run(env: WorkEnvironment):
        try:
            ensure_project_existence(
                environment=env,
                organization=arguments.organization,
                folder=arguments.folder,
                credentials=gcp_credentials
            )
            associate_billing_account(
                environment=env,
                billing_account=arguments.billing_account,
                credentials=gcp_credentials
            )
            return f'{env.email}{arguments.separator}{env.project}'
        except Exception as e:
            print(f'❌ Failed to setup project for user {env.email}: {e}')
            return None

    result = pool.map(run, environments)

    with open(os.path.abspath(arguments.output_path), 'w') as stream:
        stream.write('\n'.join(r for r in result if r is not None))

