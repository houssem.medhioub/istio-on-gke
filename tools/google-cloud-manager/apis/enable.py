#!/usr/bin/env python3
import os
import argparse
from multiprocessing.dummy import Pool as ThreadPool

from googleapiclient import discovery
from google.oauth2 import service_account


def enable_apis(
        project_id: str,
        apis: [str],
        credentials: service_account.credentials.Credentials = None
):
    service = discovery.build('serviceusage', 'v1', credentials=credentials)
    apis_request = service.services().batchEnable(
        parent=f'projects/{project_id}',
        body={
            'serviceIds': apis,
        }
    )
    print(f'👷 About to enable {len(apis)} api(s) on project "{project_id}"')
    apis_request.execute()
    print('✅ Done')


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='''
            Enable APIs on given project(s)
        '''
    )
    parser.add_argument(
        dest='input_path',
        help='Path to input CSV file',
    )
    parser.add_argument(
        '--project-col',
        dest='project_col',
        help='Index of the project column. Defaults to 0',
        default=0,
        type=int
    )
    parser.add_argument(
        '--separator',
        dest='separator',
        help='Separator to use to parse the CSV. Defaults to ","',
        default=','
    )
    parser.add_argument(
        '-a', '--apis',
        action='store',
        dest='apis',
        type=str,
        nargs='+',
        default=[],
        required=True,
        help="APIs to enable. Example of usage: -a container.googleapis.com cloudtrace.googleapis.com"
    )
    parser.add_argument(
        '-s', '--service-account-key',
        dest='service_account_key',
        help='Path to the GCP service account key to use',
        required=True
    )
    parser.add_argument(
        '-t', '--threads',
        dest='threads',
        help='Number of threads to use to parallelize requests. Defaults to 5',
        type=int,
        default=5
    )
    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_arguments()

    with open(os.path.abspath(arguments.input_path), 'r') as stream:
        content = stream.readlines()

    gcp_credentials = service_account.Credentials.from_service_account_file(
        filename=os.path.abspath(arguments.service_account_key)
    )

    pool = ThreadPool(arguments.threads)
    projects = [raw.strip().split(arguments.separator)[arguments.project_col] for raw in content]

    def run(project: str):
        try:
            enable_apis(
                project_id=project,
                apis=arguments.apis,
                credentials=gcp_credentials,
            )
        except Exception as e:
            print(f'❌ Failed to enable APIs on project "{project}": {e}')

    pool.map(run, projects)
